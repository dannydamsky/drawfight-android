The Android client for the DrawFight game. All rights reserved to John Bryce students of the Android Developer course (33rd period guided by Tsur Ben Ami)


[View the back-end Java server application repository](https://bitbucket.org/dannydamsky/drawfight-server)


[Download the game dev plan (Written in Hebrew)](https://bitbucket.org/dannydamsky/drawfight-android/downloads/DrawFight.docx)