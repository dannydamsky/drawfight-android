package com.john.bryce.df.drawfight.ui.fragments.chat.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.john.bryce.df.drawfight.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

    private Context context;




    public ChatAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.row_chat , viewGroup, false);
        return new ChatViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder chatViewHolder, int i) {
        chatViewHolder.onBind( "sdfd" );
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message_content)
        TextView messageContent;
        @BindView(R.id.message_user_name)
        TextView messageUserName;

        public ChatViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );
        }

        public void onBind(final String str){
            messageContent.setText( str );
            messageUserName.setText( str );

        }
    }
}
