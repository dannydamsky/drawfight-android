package com.john.bryce.df.drawfight.util.canvas;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.view.MotionEvent;

public final class CanvasMovement {
    public static CanvasMovement newInstance(@NonNull MotionEvent event, @NonNull Paint mPaint) {
        CanvasMovement movement = new CanvasMovement();
        movement.setMovementAction(event.getAction());
        movement.setPaintColor(mPaint.getColor());
        movement.setStrokeWidth(mPaint.getStrokeWidth());
        movement.setX(event.getX());
        movement.setY(event.getY());
        return movement;
    }

    private int paintColor;
    private float strokeWidth;
    private int movementAction;
    private float x;
    private float y;

    public CanvasMovement() {
    }

    public CanvasMovement(int paintColor, float strokeWidth, int movementAction, float x, float y) {
        this.paintColor = paintColor;
        this.strokeWidth = strokeWidth;
        this.movementAction = movementAction;
        this.x = x;
        this.y = y;
    }

    public int getPaintColor() {
        return paintColor;
    }

    public void setPaintColor(int paintColor) {
        this.paintColor = paintColor;
    }

    public float getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public int getMovementAction() {
        return movementAction;
    }

    public void setMovementAction(int movementAction) {
        this.movementAction = movementAction;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "CanvasMovement{" +
                "paintColor=" + paintColor +
                ", strokeWidth=" + strokeWidth +
                ", movementAction=" + movementAction +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
