package com.john.bryce.df.drawfight.ui.fragments.game;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.john.bryce.df.drawfight.R;
import com.john.bryce.df.drawfight.ui.widget.CanvasView;
import com.john.bryce.df.drawfight.ui.widget.LetterButton;
import com.john.bryce.df.drawfight.ui.widget.LetterGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentGame extends Fragment implements LetterGroup.CallBack {

    public static final String TAG = "com.john.bryce.df.drawfight.ui.fragments.game.fragmentgame.TAG";
    public static String wordStatus = "";
    private boolean eraserFlag = true;
    @BindView(R.id.canvas_view)
    CanvasView canvasView;
    @BindView(R.id.user_photo)
    ImageView userPhoto;
    @BindView(R.id.player_name)
    TextView playerName;
    @BindView(R.id.player_score)
    TextView playerScore;
    @BindView(R.id.fragment_game)
    ConstraintLayout constraintLayout;
    @BindView(R.id.letterGroupWord)
    ConstraintLayout letterGroupWord;
    @BindView(R.id.letterGroupOption)
    ConstraintLayout letterGroupOption;
    @BindView(R.id.eraser)
    ImageButton eraser;

    private LetterGroup letterWord;
    private LetterGroup letterOption;

    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayout) {
        final Bundle bundle = new Bundle();
        FragmentGame fragmentGame = new FragmentGame();
        fragmentGame.setArguments( bundle );
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack( TAG ).replace( parentLayout, fragmentGame, TAG ).commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_game, container, false );
        ButterKnife.bind( this, view );
        letterWord = new LetterGroup( getContext() );
        letterOption = new LetterGroup( getContext() );
        letterOption.setCallBack( this );
        letterWord.setCallBack( this );
        letterWord.showEmptyBoxes( letterGroupWord, "game" );
        letterOption.showWordOption( letterGroupOption, "sdfkatERWKJFYBMwhq" );
        eraser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eraserClick();
            }
        });
        return view;
    }

    private void eraserClick() {
        if (eraserFlag){
            canvasView.setEraser();
            eraserFlag = false;
        }else {
            canvasView.cancelEraser();
            eraserFlag = true;
        }
    }

    //public void getWord();

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClickOnLetter(String letter, LetterButton view) {
        int x = wordStatus.length();
        if (letterWord.checkIfLetterIsRight( letter, wordStatus, "mom" )) {
            wordStatus += letter;
            view.setClickable( false );
            view.setTrueAnswerBuzzer();
        } else {
            view.setFalseAnswerBuzzer();
        }
    }



    @Override
    public void rightAnswer() {
        //send right answer
    }
}