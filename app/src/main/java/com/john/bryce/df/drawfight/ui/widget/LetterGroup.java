package com.john.bryce.df.drawfight.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.john.bryce.df.drawfight.R;

public class LetterGroup extends ConstraintLayout implements LetterButton.OnClickListener {

    int currWidth;
    private String word;
    private ConstraintLayout constraintLayout;
    private LetterButton[] buttonList;
    private CallBack callBack;

    public LetterGroup(Context context) {
        super( context );
    }

    public LetterGroup(Context context, AttributeSet attrs) {
        super( context, attrs );
    }

    public LetterGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super( context, attrs, defStyleAttr );
    }

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public void showEmptyBoxes(@NonNull ConstraintLayout constraintLayout, @NonNull String word) {
        this.constraintLayout = constraintLayout;
        this.buttonList = new LetterButton[word.length()];
        this.word = word;
        createEmptyBoxes( word );
    }

    public void showWordOption(@NonNull ConstraintLayout constraintLayout, @NonNull String word) {
        this.constraintLayout = constraintLayout;
        this.buttonList = new LetterButton[word.length()];
        this.word = word;
        createLetterButtonsOption( word );
    }

    public void createEmptyBoxes(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (i == 0) {
                createOptionButton( i + 100, ConstraintSet.PARENT_ID, (i + 100) + 1, false, true, word.charAt( i ) + "", false );
            } else if (i == word.length() - 1) {
                createOptionButton( i + 100, (i + 100) - 1, ConstraintSet.PARENT_ID, true, false, word.charAt( i ) + "", false );

            } else {
                createOptionButton( i + 100, (i + 100) - 1, (i + 100) + 1, false, false, word.charAt( i ) + "", false );
            }
        }
    }

    public void createLetterButtonsOption(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (i == 0) {
                createOptionButton( i + 100, ConstraintSet.PARENT_ID, (i + 100) + 1, false, true, word.charAt( i ) + "", true );
            } else if (i == word.length() - 1) {
                createOptionButton( i + 100, (i + 100) - 1, ConstraintSet.PARENT_ID, true, false, word.charAt( i ) + "", true );
            } else {
                createOptionButton( i + 100, (i + 100) - 1, (i + 100) + 1, false, false, word.charAt( i ) + "", true );
            }
        }
    }

    public void createOptionButton(int id, int startId, int endId, boolean end, boolean first, String letter, boolean isClickable) {
        ConstraintSet set = new ConstraintSet();
        set.clone( constraintLayout );
        buttonList[id - 100] = new LetterButton( getContext() );
        buttonList[id - 100].setTvLetter( letter );
        buttonList[id - 100].setId( id );
        if (isClickable) {
            buttonList[id - 100].setIsClickable( true );
            buttonList[id - 100].setTextButton( letter );
            buttonList[id - 100].setOnClickListener( this );
        } else {
            buttonList[id - 100].setBackground( getResources().getDrawable( R.drawable.letter_square_background ) );
        }
        constraintLayout.addView( buttonList[id - 100] );

        DisplayMetrics dm = getContext().getResources().getDisplayMetrics();
        int width = (int) dpFromPx( getContext(), dm.widthPixels ) - 40;

        if (currWidth < width) {
            //top
            if (first) {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.TOP, R.id.cardView_canvas, ConstraintSet.BOTTOM, 0 );
            } else {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.TOP, buttonList[id - 101].getId(), ConstraintSet.TOP, 0 );
            }
            //start
            if (first) {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.START, startId, ConstraintSet.START, 4 );
            } else {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.START, startId, ConstraintSet.END, 4 );
            }
            //end
            if (end && currWidth + dpFromPx( getContext(), 100 ) < width) {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.END, endId, ConstraintSet.END, 4 );
            } else if (currWidth + dpFromPx( getContext(), 100 ) > width) {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 4 );
            } else {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.END, endId, ConstraintSet.START, 4 );
            }
            currWidth += dpFromPx( getContext(), 100 );
            //Next line
        } else {
            set.connect( buttonList[id - 100].getId(), ConstraintSet.TOP, buttonList[id - 101].getId(), ConstraintSet.BOTTOM, 4 );
            set.connect( buttonList[id - 100].getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 4 );
            if (end) {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.END, endId, ConstraintSet.END, 4 );
            } else {
                set.connect( buttonList[id - 100].getId(), ConstraintSet.END, endId, ConstraintSet.START, 4 );
            }
            currWidth = (int) dpFromPx( getContext(), 100 );
        }

        set.constrainHeight( buttonList[id - 100].getId(), 100 );
        set.constrainWidth( buttonList[id - 100].getId(), 100 );
        set.applyTo( constraintLayout );// <-- Important

    }

    public LetterButton[] getButtonList() {
        return buttonList;
    }

    public void setButtonList(LetterButton[] buttonList) {
        this.buttonList = buttonList;
    }

    public boolean checkIfLetterIsRight(String letter, String wordStatus, String word) {
        if (getButtonList()[wordStatus.length()].getTvLetter().equals( letter )) {
            getButtonList()[wordStatus.length()].setText( letter );

            if (getButtonList().length == word.length() && wordStatus.length() > 1 ) {
                callBack.rightAnswer();
            }
            return true;

        } else {
            return false;
        }
    }


    public void setCallBack(@NonNull CallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onClick(View view) {
        view.setOnClickListener( new LetterButton.OnLetterButtonClickListener() {
            @Override
            void onClick(View view, String letter) {
                callBack.onClickOnLetter( letter, (LetterButton) view );
            }
        } );
        view.callOnClick();
    }

    public interface CallBack {
        void onClickOnLetter(String letter, LetterButton view);

        void rightAnswer();
    }
}
