package com.john.bryce.df.drawfight.service;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.john.bryce.df.drawfight.data.prefs.PreferencesHelper;
import com.john.bryce.df.drawfight.util.Constants;

public final class MyFirebaseMessagingService extends FirebaseMessagingService {

    /**
     * @param s The new FCM token
     */
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        PreferencesHelper.getInstance(getApplicationContext()).putFcmToken(s);
    }

    /**
     * @param remoteMessage A new message from Firebase.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        final Intent broadcastIntent = new Intent(Constants.ACTION_NEW_FCM_MESSAGE);
        
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(broadcastIntent);
    }

}
