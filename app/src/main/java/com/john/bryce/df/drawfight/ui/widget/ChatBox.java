package com.john.bryce.df.drawfight.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.john.bryce.df.drawfight.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatBox extends ConstraintLayout {
    @BindView(R.id.sendButton)
    ImageButton sendButton;

    @BindView(R.id.et_chat)
    EditText etChat;

    public ChatBox(Context context) {
        super( context );
        bindViews();
    }

    public ChatBox(Context context, AttributeSet attrs) {
        super( context, attrs );
        bindViews();
    }

    public ChatBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super( context, attrs, defStyleAttr );
        bindViews();
    }

    public void bindViews() {
        LayoutInflater layoutInflater = LayoutInflater.from( getContext() );
        View thisView = layoutInflater.inflate( R.layout.widget_chat_box, this, true );
        ButterKnife.bind( this, thisView );
    }

    public void setSendButtonOnClickListener(View.OnClickListener listener) {
        this.sendButton.setOnClickListener( listener );
    }

    public void setSendButtonClickListener(OnSendClickListener listener) {
        this.sendButton.setOnClickListener( v -> {
            listener.onSendClick( getText().trim() );
            etChat.setText( "" );
        } );
    }

    public String getText() {
        return etChat.getText().toString();
    }

    public void setText(String text) {
        etChat.setText( text );
    }

    public EditText getMessageTextBox() {
        return etChat;
    }

    public interface OnSendClickListener {
        void onSendClick(@NonNull String text);
    }
}
