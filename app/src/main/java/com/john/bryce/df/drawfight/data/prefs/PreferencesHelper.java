package com.john.bryce.df.drawfight.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * The class that manages the Shared Preferences of this application.
 */
public final class PreferencesHelper {

    private static final String RUNTIME_SHARED_PREFERENCES_NAME = "runtime_shared_preferences";
    private static final String RUNTIME_SHARED_PREFERENCES_FCM_TOKEN = "FCM_TOKEN";

    private static PreferencesHelper instance;

    /**
     * @param context The context from where the function is called.
     * @return An instance of the PreferencesHelper class.
     */
    @NonNull
    public static PreferencesHelper getInstance(@NonNull final Context context) {
        if (instance == null) {
            synchronized (PreferencesHelper.class) {
                if (instance == null) {
                    instance = new PreferencesHelper(context);
                }
            }
        }
        return instance;
    }

    private final SharedPreferences runtimeSharedPreferences;

    private PreferencesHelper(@NonNull final Context context) {
        this.runtimeSharedPreferences = context.getSharedPreferences(RUNTIME_SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
    }

    /**
     * @param fcmToken The new FCM token of the device.
     */
    public void putFcmToken(@NonNull final String fcmToken) {
        this.runtimeSharedPreferences.edit()
                .putString(RUNTIME_SHARED_PREFERENCES_FCM_TOKEN, fcmToken)
                .apply();
    }

    /**
     * @return The device's FCM token, or null if the token wasn't set.
     */
    @Nullable
    public String getFcmToken() {
        return this.runtimeSharedPreferences.getString(RUNTIME_SHARED_PREFERENCES_FCM_TOKEN, null);
    }

}
