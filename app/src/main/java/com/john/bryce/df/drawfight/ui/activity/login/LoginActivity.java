package com.john.bryce.df.drawfight.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.SignInButton;
import com.john.bryce.df.drawfight.R;
import com.john.bryce.df.drawfight.ui.activity.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    @BindView( R.id.sign_in_button )
    SignInButton signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );
        ButterKnife.bind( this );
       /* SignInButton signInButton = findViewById( R.id.sign_in_button );
        signInButton.setSize( SignInButton.SIZE_WIDE );*/
    }

    @OnClick(R.id.sign_in_button)
    public void onClick() {
        Intent intent = new Intent( this, MainActivity.class );
        startActivity( intent );
    }

}
