package com.john.bryce.df.drawfight.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.john.bryce.df.drawfight.R;

import butterknife.BindView;

public class LetterButton extends android.support.v7.widget.AppCompatButton {

    @BindView(R.id.tvLetter)
    TextView tvLetter;

    private String letter = "";
    /**
     * false - if the button is used in the question.
     * true - if it's used in the keyboard
     */
    private boolean isClickable = false;

    private OnLetterButtonClickListener mOnClickListener;

    public LetterButton(Context context) {
        super( context );
    }

    public LetterButton(Context context, AttributeSet attrs) {
        super( context, attrs );
    }

    public LetterButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super( context, attrs, defStyleAttr );
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener( l );
        try {
            this.mOnClickListener = (OnLetterButtonClickListener) l;
            this.mOnClickListener.setLetter( this.letter );
        } catch (ClassCastException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public String getTvLetter() {
        return letter;
    }

    public void setTvLetter(String tvLetter) {
        this.letter = tvLetter;
        if (this.mOnClickListener != null) {
            this.mOnClickListener.setLetter( this.letter );
        }
    }

    public void setTextButton(String letter) {
        if (letter.length() > 1)
            throw new IllegalStateException();
        if (letter.length() == 0) {
            this.isClickable = false;
        } else {
            this.letter = letter;
            setTvLetter( letter );
            setText( letter );
        }

    }

    public boolean isClickable() {
        return isClickable;
    }

    public Drawable getSelectedItemDrawable() {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray ta = getContext().obtainStyledAttributes( attrs );
        Drawable selectedItemDrawable = ta.getDrawable( 0 );
        ta.recycle();
        return selectedItemDrawable;
    }


    public void setIsClickable(boolean clickable) {
        if (clickable) {
            setFocusable( true );
            setClickable( true );
            setBackgroundResource(android.R.drawable.btn_default );
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                setForeground( getSelectedItemDrawable() );
            }
        } else {
            setFocusable( false );
            setClickable( false );
        }

        isClickable = clickable;
    }

    public void setFalseAnswerBuzzer() {
        setBackground( getResources().getDrawable( R.drawable.letter_square_worng_answer ) );
        final Handler handler = new Handler(  );
        handler.postDelayed( new Runnable() {
            @Override
            public void run() {
                setBackgroundResource(android.R.drawable.btn_default );
            }
        }, 1000);
    }

    public void setTrueAnswerBuzzer() {
        setBackground( getResources().getDrawable( R.drawable.letter_square_true_answer ) );
    }

    public static abstract class OnLetterButtonClickListener implements View.OnClickListener {

        private String letter;

        private void setLetter(String letter) {
            this.letter = letter;
        }

        @Override
        public final void onClick(View view) {
            onClick( view, letter );
        }

        abstract void onClick(View view, String letter);
    }
}
