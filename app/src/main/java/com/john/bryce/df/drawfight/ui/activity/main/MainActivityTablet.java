package com.john.bryce.df.drawfight.ui.activity.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.john.bryce.df.drawfight.ui.activity.ActivityCallBacks;

public class MainActivityTablet implements ActivityCallBacks {

    private final MainActivity mMainActivity;

    public MainActivityTablet(MainActivity mMainActivity) {
        this.mMainActivity = mMainActivity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onRestart() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onBottomSheetExpanded() {

    }

    @Override
    public void onBottomSheetCollapsed() {

    }


    @Override
    public void onClickChatFab() {

    }

}
