package com.john.bryce.df.drawfight.ui.fragments.chat.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.john.bryce.df.drawfight.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuickMessageAdapter extends RecyclerView.Adapter<QuickMessageAdapter.QuickMessageViewHolder> {

    private Context context;

    public QuickMessageAdapter(@NonNull Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public QuickMessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.row_quick_box, viewGroup, false );
        return new QuickMessageViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull QuickMessageViewHolder quickMessageViewHolder, int i) {
        quickMessageViewHolder.onBind( "test" );
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class QuickMessageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_view)
        TextView text;

        public QuickMessageViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );
        }

        public void onBind(String str) {
            text.setText( str );
        }
    }
}
