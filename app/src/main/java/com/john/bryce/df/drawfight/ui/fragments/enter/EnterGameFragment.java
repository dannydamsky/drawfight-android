package com.john.bryce.df.drawfight.ui.fragments.enter;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.john.bryce.df.drawfight.R;
import com.john.bryce.df.drawfight.ui.fragments.game.FragmentGame;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnterGameFragment extends Fragment {

    public static final String TAG = "com.john.bryce.df.drawfight.entergamefragment.TAG";
    public static ArrayList<String> randomQuestions = new ArrayList<>();
    //0 = game already run
    private static long timeInMillisFromServer = 0;
    @BindView(R.id.start_game_button)
    Button startGameButton;
    @BindView(R.id.progressBarCircle)
    ProgressBar timerProgressBar;
    @BindView(R.id.textViewTime)
    TextView textViewTime;
    @BindView(R.id.timerStatus)
    TextView timerStatus;
    @BindView(R.id.get_random_questions)
    Button joinGameBtn;
    private CountDownTimer countDownTimer;

    private boolean activeGame = false;


    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayoutId) {
        final Bundle bundle = new Bundle();
        final EnterGameFragment gameFragment = new EnterGameFragment();
        gameFragment.setArguments( bundle );
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack( TAG )
                .replace( parentLayoutId, gameFragment, TAG )
                .commit();
    }

    @OnClick(R.id.start_game_button)
    public void onClickButton() {
        //tmp
        startGame();
        //getTimeInMilliSecond();
        //getActiveGame();
        if (activeGame == true) {
            Snackbar.make( getView(), getResources().getString( R.string.There_is_an_active_game_message ), Snackbar.LENGTH_LONG ).show();
            return;
        }
        if (timeInMillisFromServer >= System.currentTimeMillis()) {
            showTimer();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //initializeVisibilityOfProgressbar( View.GONE );
    }

    private void showTimer() {
        //getTimeInMilliSecond();
        initializeVisibilityOfProgressbar( View.VISIBLE );
        long g = timeInMillisFromServer - System.currentTimeMillis();

        countDownTimer = new CountDownTimer( 300000 - g, 1000 ) {
            @Override
            public void onTick(long l) {
                timerStatus.setText( "Initializing the users.." );
                textViewTime.setText( hmsTimeFormatter( l ) );
                timerProgressBar.setProgress( (int) (l / 1000) );
            }

            @Override
            public void onFinish() {
                timerStatus.setText( "Starting the game.." );
                startGame();
            }
        };
        countDownTimer.start();
    }

    private void startGame() {
        //pushStartGame();
        if (true) {
            FragmentGame.show( getFragmentManager(), R.id.container );
        }
    }

    @OnClick(R.id.get_random_questions)
    public void onClickJoinGameBtn() {
        //push to server
    }


    public void initializeVisibilityOfProgressbar(int i) {
        timerProgressBar.setVisibility( i );
        timerStatus.setVisibility( i );
        textViewTime.setVisibility( i );
    }

    /**
     * method to convert millisecond to time format
     *
     * @param milliSeconds
     * @return HH:mm:ss time formatted string
     */
    private String hmsTimeFormatter(long milliSeconds) {
        String hms = String.format( "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes( milliSeconds ) - TimeUnit.HOURS.toMinutes( TimeUnit.MILLISECONDS.toHours( milliSeconds ) ),
                TimeUnit.MILLISECONDS.toSeconds( milliSeconds ) - TimeUnit.MINUTES.toSeconds( TimeUnit.MILLISECONDS.toMinutes( milliSeconds ) ) );

        return hms;

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_enter_game, container, false );
        ButterKnife.bind( this, view );
        return view;
    }
}
