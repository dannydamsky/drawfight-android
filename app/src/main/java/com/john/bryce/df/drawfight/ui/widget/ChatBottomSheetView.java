package com.john.bryce.df.drawfight.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.john.bryce.df.drawfight.R;
import com.john.bryce.df.drawfight.ui.fragments.chat.adapters.ScoreResultAdapter;
import com.john.bryce.df.drawfight.ui.fragments.chat.adapters.ChatAdapter;
import com.john.bryce.df.drawfight.ui.fragments.chat.adapters.QuickMessageAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatBottomSheetView extends ConstraintLayout implements View.OnLayoutChangeListener {

    @BindView(R.id.chat_box)
    ChatBox chatBox;
    @BindView(R.id.rv_quick_answers)
    RecyclerView quickAnswers;
    @BindView(R.id.rv_chet_screen)
    RecyclerView chetScreen;
    @BindView(R.id.rv_best_scores)
    RecyclerView bestScores;
    @BindView(R.id.maximizedLayout)
    ConstraintLayout maximizedLayout;
    private ChatAdapter chatAdapter;
    private QuickMessageAdapter messageAdapter;
    private ScoreResultAdapter resultAdapter;
    private Callback callback;
    private BottomSheetBehavior bottomSheetBehavior;
    private boolean chatOnly = false;
    private boolean swipeUp = false;



    public ChatBottomSheetView(Context context) {
        super( context );
        bindView();
    }

    public ChatBottomSheetView(Context context, AttributeSet attrs) {
        super( context, attrs );
        bindView();
    }

    public ChatBottomSheetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super( context, attrs, defStyleAttr );
        bindView();
    }

    public void bindView() {

        final View layout = inflate( getContext(), R.layout.widget_bottom_sheet_chat, this );
        ButterKnife.bind( this, layout );
        initializeScoresRecyclerView();
        initializeQuickMessageRecyclerView();
        initializeChatRecyclerView();

    }

    public void from(@NonNull ChatBottomSheetView view) {
        bottomSheetBehavior = BottomSheetBehavior.from( view );
        bottomSheetBehavior.setState( BottomSheetBehavior.STATE_COLLAPSED );
        addOnLayoutChangeListener( this );
        setBottomSheetCallback();
    }

    public void setMaximizedViews() {
        chatBox.setVisibility( VISIBLE );
        maximizedLayout.setVisibility( VISIBLE );

    }

    private void setMinimizedLayoutViewsVisible() {
        maximizedLayout.setVisibility( GONE );
        chatBox.setVisibility( GONE );
    }

    public void setCallback(@NonNull Callback callback) {
        this.callback = callback;
    }

    private void setBottomSheetCallback() {
        bottomSheetBehavior.setBottomSheetCallback( new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        collapse();
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        onStateDragging();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        //onStateExpanded();
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        //onStateHidden();
                        break;
                }
            }
            @Override
            public void onSlide(@NonNull View view, float v) {
            }
        } );
    }

    private void onStateDragging() {
            if(chatOnly)
            setMaximizedViews();
            chatOnly = false;
    }

    public void collapse() {
        swipeUp = true;
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            setMinimizedLayoutViewsVisible();
            chatBox.setVisibility( VISIBLE );
            ConstraintSet set = new ConstraintSet();
            set.clone( chatBox );
            //set.
            //chatOnly = true;
           /* if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState( BottomSheetBehavior.STATE_EXPANDED );
            }*/
        }
    }

    public void onStateExpanded() {
        swipeUp = true;
        if(chatOnly){
            chatOnly = false;
            return;
        }
        //setMaximizedViews();
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            //bottomSheetBehavior.setState( BottomSheetBehavior.STATE_EXPANDED );
            callback.onBottomSheetExpanded();
        }
    }

    public void onStateHidden() {
        bottomSheetBehavior.setState( BottomSheetBehavior.STATE_HIDDEN );
        swipeUp = false;
        //setMinimizedLayoutViewsVisible();
    }

    public void onClickFab(){
        if(!swipeUp){
            swipeUp = true;
            setMaximizedViews();
            bottomSheetBehavior.setState( BottomSheetBehavior.STATE_EXPANDED );
        }else {
            onStateHidden();
        }
    }

    public void initializeChatRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getContext() );
        chetScreen.setLayoutManager( layoutManager );
        chetScreen.setHasFixedSize( true );
        chatAdapter = new ChatAdapter( getContext() );
        chetScreen.setAdapter( chatAdapter );
    }

    public void initializeScoresRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getContext() );
        bestScores.setLayoutManager( layoutManager );
        bestScores.setHasFixedSize( true );
        resultAdapter = new ScoreResultAdapter( getContext() );
        bestScores.setAdapter( resultAdapter );
    }

    public void initializeQuickMessageRecyclerView() {
        quickAnswers.setLayoutManager( new LinearLayoutManager( getContext(), LinearLayout.HORIZONTAL, true ) );
        quickAnswers.setHasFixedSize( true );
        messageAdapter = new QuickMessageAdapter( getContext() );
        quickAnswers.setAdapter( messageAdapter );
    }

    @Override
    public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {

    }

    public interface Callback {


        void onBottomSheetExpanded();

        void onBottomSheetCollapsed();

    }


}



