package com.john.bryce.df.drawfight.ui.activity.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.john.bryce.df.drawfight.ui.activity.ActivityCallBacks;
import com.john.bryce.df.drawfight.ui.widget.ChatBottomSheetView;

import butterknife.OnClick;

public final class MainActivityPhoneIml implements ActivityCallBacks {

    private final MainActivity mMainActivity;

    public MainActivityPhoneIml(MainActivity mainActivity) {
        this.mMainActivity = mainActivity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        initPlacesBottomSheetView();
        //FragmentChat.show( mMainActivity.getSupportFragmentManager(), R.id.container );
    }

    @Override
    public void onStart() {

    }



    private void initPlacesBottomSheetView() {
        mMainActivity.mChatBottomSheetView.from( mMainActivity.mChatBottomSheetView );
        mMainActivity.mChatBottomSheetView.setCallback( mMainActivity );
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onRestart() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onBottomSheetExpanded() {

    }

    @Override
    public void onBottomSheetCollapsed() {

    }


    @Override
    public void onClickChatFab() {
        this.mMainActivity.mChatBottomSheetView.onClickFab();
    }

}
