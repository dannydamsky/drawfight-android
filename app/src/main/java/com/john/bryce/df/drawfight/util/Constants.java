package com.john.bryce.df.drawfight.util;

public final class Constants {

    private Constants() {
    }

    public static final String ACTION_NEW_FCM_MESSAGE = "com.john.bryce.df.ACTION_NEW_FCM_MESSAGE";

}
