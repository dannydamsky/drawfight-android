package com.john.bryce.df.drawfight.util.canvas;

import java.util.ArrayList;
import java.util.List;

public final class CanvasMessage {
    private List<CanvasMovement> movements = new ArrayList<>();
    private int viewHeight;
    private int viewWidth;

    public CanvasMessage() {
    }

    public CanvasMessage(List<CanvasMovement> movements, int viewHeight, int viewWidth) {
        this.movements = movements;
        this.viewHeight = viewHeight;
        this.viewWidth = viewWidth;
    }

    public CanvasMessage(int viewHeight, int viewWidth) {
        this.viewHeight = viewHeight;
        this.viewWidth = viewWidth;
    }

    public void addMovement(CanvasMovement movement) {
        movements.add(movement);
    }

    public void clearMovements() {
        movements.clear();
    }

    public List<CanvasMovement> getMovements() {
        return movements;
    }

    public void setMovements(List<CanvasMovement> movements) {
        this.movements = movements;
    }

    public int getViewHeight() {
        return viewHeight;
    }

    public void setViewHeight(int viewHeight) {
        this.viewHeight = viewHeight;
    }

    public int getViewWidth() {
        return viewWidth;
    }

    public void setViewWidth(int viewWidth) {
        this.viewWidth = viewWidth;
    }

    @Override
    public String toString() {
        return "CanvasMessage{" +
                "movements=" + movements +
                ", viewHeight=" + viewHeight +
                ", viewWidth=" + viewWidth +
                '}';
    }
}
