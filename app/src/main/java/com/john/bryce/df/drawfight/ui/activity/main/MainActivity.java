package com.john.bryce.df.drawfight.ui.activity.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;

import com.john.bryce.df.drawfight.R;
import com.john.bryce.df.drawfight.ui.activity.ActivityCallBacks;
import com.john.bryce.df.drawfight.ui.fragments.enter.EnterGameFragment;
import com.john.bryce.df.drawfight.ui.widget.ChatBottomSheetView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements ChatBottomSheetView.Callback {

    @BindView(R.id.design_bottom_sheet)
    ChatBottomSheetView mChatBottomSheetView;

    @BindView( R.id.chat_fab )
    FloatingActionButton chatFab;

    private ActivityCallBacks mActivityCallBacks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        ButterKnife.bind( this );
        EnterGameFragment.show(getSupportFragmentManager(), R.id.container);
        //initPlacesBottomSheetView();
        if (findViewById( R.id.largeScreen ) != null) {
            this.mActivityCallBacks = new MainActivityTablet( this );
        } else {
            this.mActivityCallBacks = new MainActivityPhoneIml(this);
        }
        this.mActivityCallBacks.onCreate( savedInstanceState );
    }

    @OnClick(R.id.chat_fab)
    public void onClickChatFab(){
        this.mActivityCallBacks.onClickChatFab();
    }



    @Override
    public void onBottomSheetExpanded() {
        this.mActivityCallBacks.onBottomSheetExpanded(  );
    }

    @Override
    public void onBottomSheetCollapsed() {
            this.mActivityCallBacks.onBottomSheetCollapsed();
    }



}
