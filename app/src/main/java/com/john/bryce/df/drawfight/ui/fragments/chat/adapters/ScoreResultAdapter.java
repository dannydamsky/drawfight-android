package com.john.bryce.df.drawfight.ui.fragments.chat.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.john.bryce.df.drawfight.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScoreResultAdapter extends RecyclerView.Adapter<ScoreResultAdapter.ScoreResultViewHolder> {

    private Context context;

    public ScoreResultAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ScoreResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.row_score_result, viewGroup, false );
        return new ScoreResultViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ScoreResultViewHolder scoreResultViewHolder, int i) {
        scoreResultViewHolder.onBind( "dmdmd" );
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ScoreResultViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.score_result_name)
        TextView scoreName;
        @BindView(R.id.score_result_scores)
        TextView scores;
        @BindView( R.id.icon )
        ImageView icon;

        public ScoreResultViewHolder(@NonNull View itemView) {
            super( itemView );
            ButterKnife.bind( this, itemView );
        }

        public void onBind(String str) {
            scoreName.setText( str );
            scores.setText( str );
            icon.setBackgroundResource(  R.drawable.first_place_icon );

        }
    }
}
