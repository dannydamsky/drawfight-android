package com.john.bryce.df.drawfight.ui.fragments.chat;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.john.bryce.df.drawfight.R;
import com.john.bryce.df.drawfight.ui.fragments.chat.adapters.ScoreResultAdapter;
import com.john.bryce.df.drawfight.ui.fragments.chat.adapters.ChatAdapter;
import com.john.bryce.df.drawfight.ui.fragments.chat.adapters.QuickMessageAdapter;
import com.john.bryce.df.drawfight.ui.widget.ChatBox;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentChat extends Fragment {
    public static final String TAG = "com.john.bryce.df.drawfight.fragmentchat.TAG";
    @BindView(R.id.chat_box)
    ChatBox chatBox;
    @BindView(R.id.rv_quick_answers)
    RecyclerView quickAnswers;
    @BindView(R.id.rv_chet_screen)
    RecyclerView chetScreen;
    @BindView(R.id.rv_best_scores)
    RecyclerView bestScores;
    private ChatAdapter chatAdapter;
    private QuickMessageAdapter messageAdapter;
    private ScoreResultAdapter resultAdapter;
    /*@BindView( R.id.design_bottom_sheet )
    ChatBottomSheetView chatBottomSheetView;*/

    public static void show(@NonNull FragmentManager manager, @IdRes int parentLayoutId) {
        final Bundle bundle = new Bundle();
        final FragmentChat fragmentChat = new FragmentChat();
        fragmentChat.setArguments( bundle );
        final FragmentTransaction transaction = manager.beginTransaction();
        transaction.addToBackStack( TAG );
        transaction.replace( parentLayoutId, fragmentChat, TAG );
        transaction.commit();
    }

    public void initializeChatRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getContext() );
        chetScreen.setLayoutManager( layoutManager );
        chetScreen.setHasFixedSize( true );
        chatAdapter = new ChatAdapter( getContext() );
        chetScreen.setAdapter( chatAdapter );
    }

    public void initializeScoresRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getContext() );
        bestScores.setLayoutManager( layoutManager );
        bestScores.setHasFixedSize( true );
        resultAdapter = new ScoreResultAdapter( getContext() );
        bestScores.setAdapter( resultAdapter );
    }

    public void initializeQuickMessageRecyclerView() {
        quickAnswers.setLayoutManager( new LinearLayoutManager( getContext(), LinearLayout.HORIZONTAL, true ) );
        quickAnswers.setHasFixedSize( true );
        messageAdapter = new QuickMessageAdapter( getContext() );
        quickAnswers.setAdapter( messageAdapter );
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_chat, container, false );
        ButterKnife.bind( this, view );
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeChatRecyclerView();
        initializeQuickMessageRecyclerView();
        initializeScoresRecyclerView();

    }
}
