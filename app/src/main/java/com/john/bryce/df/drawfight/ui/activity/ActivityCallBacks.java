package com.john.bryce.df.drawfight.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface ActivityCallBacks {

    void onCreate(@Nullable Bundle savedInstanceState);

    void onStart();

    void onStop();

    void onResume();

    void onRestart();

    void onDestroy();

    void onBackPressed();

    void onPause();

    void onBottomSheetExpanded();

    void onBottomSheetCollapsed();

    void onClickChatFab();


}
