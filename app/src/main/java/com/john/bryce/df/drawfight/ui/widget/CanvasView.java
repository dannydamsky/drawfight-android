package com.john.bryce.df.drawfight.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.john.bryce.df.drawfight.util.canvas.CanvasMessage;
import com.john.bryce.df.drawfight.util.canvas.CanvasMovement;

import java.util.List;


public class CanvasView extends View {

    private final Path mPath = new Path();
    private final Paint mBitmapPaint = new Paint( Paint.DITHER_FLAG );
    private Paint mPaint = new Paint();
    private Canvas mCanvas;
    private Bitmap mBitmap;

    private boolean mActionDown = false;


    private CanvasMessage canvasMessage = new CanvasMessage();


    public CanvasView(Context context) {
        super( context );
        initializePaint();
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs) {
        super( context, attrs );
        initializePaint();
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super( context, attrs, defStyleAttr );
        initializePaint();
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super( context, attrs, defStyleAttr, defStyleRes );
        initializePaint();
    }

    private void initializePaint() {
        mPaint.setAntiAlias( true );
        mPaint.setColor( getResources().getColor( android.R.color.black ) );
        mPaint.setStrokeWidth( 5f );
        mPaint.setStrokeJoin( Paint.Join.ROUND );
        mPaint.setStyle( Paint.Style.STROKE );
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged( w, h, oldw, oldh );
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw( canvas );
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        CanvasMovement movement = CanvasMovement.newInstance(event, mPaint);
        canvasMessage.addMovement(movement);
        return onTouchEvent(event.getAction(), event.getX(), event.getY());
    }
//    public void setEraser(){
//        mPaint.setColor(Color.WHITE);
//    }

    private boolean onTouchEvent(int action, float x, float y) {
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mPath.moveTo(x, y);
                mActionDown = true;
                break;
            case MotionEvent.ACTION_MOVE:
                mPath.lineTo(x, y);
                mActionDown = false;
                break;
            case MotionEvent.ACTION_UP:
                mPath.lineTo(x, y);
                mCanvas.drawPath(mPath, mPaint);
                mPath.reset();
                if (mActionDown) {
                    performClick();
                }
                break;
        }
        invalidate();
        return true;
    }

    public void clearCanvas() {
        mActionDown = false;
        canvasMessage.clearMovements();
        mPath.reset();
        invalidate();
    }

    @Override
    public boolean performClick() {
        mActionDown = false;
        return super.performClick();
    }

    public void setPaintWidth(int paintWidth) {
        mPaint.setStrokeWidth(paintWidth);
    }


    public void setPaintColor(@ColorInt int paintColor) {
        mPaint.setColor(paintColor);
    }

    public void setEraser() {
        mPaint.setColor(getResources().getColor(android.R.color.white));
        mPaint.setStrokeWidth(20f);
    }
    public void cancelEraser(){
        mPaint.setColor(getResources().getColor(android.R.color.black));
        mPaint.setStrokeWidth(5f);

    }

    public void setCanvasMessage(@NonNull CanvasMessage message) {
        this.canvasMessage.setMovements(message.getMovements());
        List<CanvasMovement> movements = message.getMovements();
        int length = movements.size();
        for (int i = 0; i < length; i++) {
            setupNewMovement(movements.get(i), message);
        }
        initializePaint();
    }

    private void setupNewMovement(CanvasMovement movement, CanvasMessage message) {
        mPaint.setColor(movement.getPaintColor());
        float strokeWidth = getCalculationByFormulaXy(movement.getStrokeWidth(), getWidth(), message.getViewWidth());
        float x = getCalculationByFormulaXy(movement.getX(), getWidth(), message.getViewWidth());
        float y = getCalculationByFormulaXy(movement.getY(), getHeight(), message.getViewHeight());
        movement.setX(x);
        movement.setY(y);
        movement.setStrokeWidth(strokeWidth);
        mPaint.setStrokeWidth(movement.getStrokeWidth());
        onTouchEvent(movement.getMovementAction(), x, y);
    }

    private float getCalculationByFormulaXy(float dot, int currentWidth, int originalWidth) {
        double dotD = (double) dot;
        double cW = (double) currentWidth;
        double oW = (double) originalWidth;
        double calculation = (dotD * cW) / oW;
        return (float) calculation;
    }

    @NonNull
    public CanvasMessage getCanvasMessage() {
        return this.canvasMessage;
    }

    @ColorInt
    public int getStrokeColor() {
        return mPaint.getColor();
    }

    public float getStrokeWidth() {
        return mPaint.getStrokeWidth();
    }
}
